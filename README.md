# mtk-apusys-driver

APUSYS is a proprietary hardware in Mediatek SoC to support AI operations.

This repo is out-of-tree kernel driver of APUSYS. 



## User space

In user space, APUSYS works with the Mediatek Neuron SDK to provides user with powerful tools for compiling their Neural Network models and executing them with hardware acceleration.



## Branch

- main: main branch is unused.
- android13: Current active. For G700 and G1200 platforms, neuropilot version 6.0. The kernel driver is compatible with [mtk-v5.15-dev](https://gitlab.com/mediatek/aiot/bsp/linux/-/tree/mtk-v5.15-dev) and it is still under development and subject to design changes.
- mt8195: Deprecated. For G1200 platform, neuropilot version 5.0. The kernel driver is compatible with [mtk-v5.15-dev](https://gitlab.com/mediatek/aiot/bsp/linux/-/tree/mtk-v5.15-dev).
- mt8188: Deprecated. For G700 platform, neuropilot version 6.0. The kernel driver is compatible with [mtk-v5.15-dev](https://gitlab.com/mediatek/aiot/bsp/linux/-/tree/mtk-v5.15-dev).

